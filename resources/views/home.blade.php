@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <a href="/posts/create" class="btn btn-primary">Create Post</a>
                   <h3>Your blog post!</h3>
                   @if(count($posts) > 0)
                    <table class="table table-striped">
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td class="text-nowrap text-center">
                                <div class="btn-group inline text-center">
                                    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary" role="button">Edit</a>                                
                                </div>
                                <div class="btn-group inline text-center">
                                    {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class'=>''])!!}
                                        {{Form::hidden('_method', 'DELETE')}}
                                        {{Form::submit('Delete', ['class'=> 'btn btn-danger'])}}
                                    {!!Form::close()!!}
                                </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                   @else
                    <p>You have no Posts.</p>
                   @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
