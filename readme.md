## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

#Blog Laravel#
This is my first blog web applicatioin builded by Laravel Framework

##What is Laravel?##
[x] Open source PHP framework
[x] Aims to make dev process pleasing without sacrificing quality
[x] one of the most popular and respected PHP frameworks
[x] uses the MVC (Model View Controller Design Pattern

##What does Laravel do?##
[x] Route Handling
[x] Security Layer
[x] Models & DB Migration
[x] Views/Templates
[x] Authentication
[x] Sessions
[x] Compile Assets
[x] Storage & file management
[x] Error Handling
[x] Unit testing
[x] Email / Config
[x] Cache Handling

##Installing Laravel ##
Laravel is installed using composer
[x] Download composer from http://getcomposer.org
[x] Install Composer
    $ composer create-proect laravel/laravel myapp
[x] add V-host and hosts file entry

##Artisan CLI##
Laravel includes the Artisan CLI which handles many tasks
[x] Creating controller & models
[x] Creating database migration files and running migrations
[x] Create providers, events, jobs, form request, etc
[x] Show routes
[x] Session commands
[x] Run tinker
[x] Create custom commands

##Examples of Arisan Commands##
$ php artisan list
$ php artisan help migrate
$ php artisan make:controller TodosController
$ php artisan maek:controller TodosController --resource
$ php artisan make:model Todo -m
$ php artisan make:migration add_todos_to_db-table=todos
$ php artisan migrate
$ php artisan tinker
$ php artisan route:list
$ php artisan vendor:publish --tag=ckeditor
$ php artisan make:migration rollback
$ php artisan storage:link

##Composer running command##
$ composer require "laravelcollective/html":"^5.4.0"
$ composer require unisharp/laravel-ckeditor

##Eloquent ORM##
Laravel includes the Eloquent object relational mapper
[x] Makes querying & working with the DB very easy
[x] We can still use raw SQL queries if needed
    use App\Todo;
    $ todo = new Todo;
    $ todo->title='Some todo';
    $todo->save();

##Blade template Engine##
[x] Simple & powerfull
[x] Control structures(if else, loops, etc)
[x] Can use <?php echo 'PHP Tags'; ?>
[x] Template inheritance:Extend layouts easily
[x] Can create custom components

##References##
[x] [Laravel HTL](https://laravelcollective.com/docs/master/html)
[x] [Laravel TexEditor](https://packagist.org/packages/unisharp/laravel-ckeditor)    

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](http://patreon.com/taylorotwell):

- **[Vehikl](http://vehikl.com)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Styde](https://styde.net)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
